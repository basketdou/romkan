# eromkan
romkan CLI tool
## What is romkan
romkan are Romaji/Kana conversion libraries and tools
## Dependencies
*  kana library https://github.com/gojp/kana
*  A Qt binding for Go (Golang) https://github.com/therecipe/qt (required to build a GUI)

## Help message
`./romkan --help`

>  Usage of ./romkan:
>  
> --verbose        Verbose output
>  -h2k            Convert hiragana to katakana (legacy mode only)
>  -k2h            Convert katakana to hiragana (legacy mode only)
>  -k2r            Convert kana to romaji
>  -l              Use application in legacy mode
>  -r2h            Convert romaji to hiragana
>  -r2k            Convert romaji to katakana
>  -t2c string     Text to convert (works in usual (non-legacy) mode only) (default "foo")

## Q&A
**Q**: What is a legacy mode?

**A**: Legacy mode is a way of entering a text to convert into one of writing systems. It used to be the main mode in eromkan before the *2020-05-01* update.

## How to build
Download romkan.go file

Download qromkan.go file located in qromkan folder (optional) (GUI (alpha))

Execute the following commands:

`$ go env -w GO111MODULE=auto`

`$ go get github.com/gojp/kana`

`$ go build romkan.go`

`$ go build qromkan.go` (optional)
## Notes
The hiragana2katakana and katakana2hiragana conversion implementation was inspired by https://www.lemoda.net/go/hiragana-to-katakana/index.html
