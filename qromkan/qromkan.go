package main
import (
    "os"
    "strings"
    "github.com/gojp/kana"
    "github.com/therecipe/qt/widgets"
)

func main() {
    appromk := widgets.NewQApplication(len(os.Args), os.Args)
    window := widgets.NewQMainWindow(nil, 0)
    window.SetMinimumSize2(450, 400)
    window.SetWindowTitle("qromkan v1.0")
        
    widget := widgets.NewQWidget(nil, 0)
    widget.SetLayout(widgets.NewQVBoxLayout())
    window.SetCentralWidget(widget)
        
    inpfield := widgets.NewQLineEdit(nil)
    inpfield.SetPlaceholderText("Enter kana or romaji here")
    widget.Layout().AddWidget(inpfield)
        
    inptype := widgets.NewQLineEdit(nil)
    inptype.SetPlaceholderText("An input type (supported: romaji, kana, hiragana, katakana)")
    widget.Layout().AddWidget(inptype)
    
    outtype := widgets.NewQLineEdit(nil)
    outtype.SetPlaceholderText("Convert to (supported: romaji, hiragana, katakana)")
    widget.Layout().AddWidget(outtype)
        
    compbutton := widgets.NewQPushButton2("Convert", nil)
     compbutton.ConnectClicked(func(bool) {
            if inptype.Text() == "romaji" {
                if outtype.Text() == "hiragana" {
                    widgets.QMessageBox_Information(nil, "The result", romaji2hiragana(inpfield.Text()), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
                } 
                if outtype.Text() == "katakana" {
                    widgets.QMessageBox_Information(nil, "The result", romaji2katakana(inpfield.Text()), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
                }
            }
            if inptype.Text() == "kana" {
                if outtype.Text() == "romaji" {
                    widgets.QMessageBox_Information(nil, "The result", kana2romaji(inpfield.Text()), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
                }
            }
            if inptype.Text() == "hiragana" {
                if outtype.Text() == "katakana" {
                    widgets.QMessageBox_Information(nil, "The result", hiragana2katakana(inpfield.Text()), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
                }
            }
            if inptype.Text() == "katakana" {
                if outtype.Text() == "hiragana" {
                    widgets.QMessageBox_Information(nil, "The result", katakana2hiragana(inpfield.Text()), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
                }
            }
     })
     widget.Layout().AddWidget(compbutton)
     window.Show()
     appromk.Exec()
}
func katakana2hiragana(phrase string) string {
    //fmt.Println("katakana to hiragana")
    output := strings.Map(processkatakana, phrase)
    //fmt.Printf ("\n%s\n", output)
    return output
}
func hiragana2katakana(phrase string) string {
    //fmt.Println("hiragana to katakana")
    output := strings.Map(processhiragana, phrase)
    //fmt.Printf ("\n%s\n", output)
    return output
}
func processkatakana(char rune) rune {
    if (char >= 'ァ' && char <= 'ヶ') || (char >= 'ヽ' && char <= 'ヾ') {
        return char - 0x60
    }
    return char
}
func processhiragana(char rune) rune {
    if (char >= 'ぁ' && char <= 'ゖ') || (char >= 'ゝ' && char <= 'ゞ') {
        return char + 0x60
    }
    return char
}
func romaji2hiragana(phrase string) string {
    output := kana.RomajiToHiragana(phrase)
    return output
}    
func romaji2katakana(phrase string) string {
    output := kana.RomajiToKatakana(phrase)
    return output
}
func kana2romaji(phrase string) string {
    output := kana.KanaToRomaji(phrase)
    return output
}
